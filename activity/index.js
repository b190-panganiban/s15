console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	


let firstName = "John";
console.log("First Name:" + firstName);

let lastName = "Smith";
console.log("Last Name:" + lastName);

let age = "30";
console.log("Age:" + age);


let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
console.log("Hobbies");
console.log(hobbies);

let workAddress = {
    houseNumber: "32",
    street: "Washington",
    city: "Lincoln",
    state: "Nebraska"
};
console.log("Work Address");
console.log(workAddress);


let fullName = "Steve Rogers";
console.log("My full name is:" + fullName);


let myAge = 40;
console.log("My current age is:" + myAge);


let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
console.log("My friend are:");
console.log(friends);


let profile = {
    userName: "captain_america",
    fullName: "Steve Rogers",
    age: 40,
    isActive: false,
};
console.log("My full profile is:");
console.log(profile);


let bestFriend = "Bucky Barnes";
	console.log("My bestfriend is:" + bestFriend);

let lastLocation = "Arctic Ocean";
	console.log("I was found frozen in:" + lastLocation);